---
layout: post
title: State of the Homelab 2022
category: homelab
tags: networking, homelab, projects
---

My Homelab hasn't grown much in terms of hardware the last few years but has grown in terms of usability. Since I'm tearing everything down for a move this weekend, it's a good time to re-evaluate where I am in terms of hardware and services. 

## Hardware

My Homelab is very modest hardware-wise: I have two servers and a small amount of networking equipment. I have very little need to grow the hardware at this point.

### Eriador: NAS

Eriador is a re-purposed desktop PC that runs as a NAS. It has one primary pool (Media) which consists of 3x4TB WD Red drives, and a backup pool (Backup) which is just an 8TB WD MyBook that mirrors the primary pool. 

![Current Disk Usage on Eriador](/public/images/state-of-the-homelab-2022/eriador-storage.png)

I have several projects in mind for after our move that will require much more storage, so this machine is a primary candidate for an upgrade. The case it is in cannot handle any more drives, so upgrading this likely means all new hardware. 

### Rivendell: Virtualization

Rivendell is an Intel NUC that runs Proxmox. Proxmox hosts all of the services I host internally as either LXC containers or virtual machines. I still have plenty of room to grow on this NUC, it's about 50% utilized in terms of memory usage and disk space. 

![Rivendell usage](/public/images/state-of-the-homelab-2022/rivendell-usage.png)

The nice thing about Proxmox is it is clusterable by default, so expanding this machine would just entail buying another NUC and installing Proxmox. That's a long way off though, this single node should be plenty for the next few years.

### Networking

My networking stack consists of the stock Google Fiber router, a plain gigabit switch, a couple of Orbi wifi routers, and a Raspberry Pi 4 running Wireguard. I have a Qotom-Q190G4N that [ran opnsense](/homelab/2021/01/23/home-network-upgrade/) for about 2 months before the box completely died. I haven't been able to figure out what happened there, but I would like to revisit that in the next apartment. The Orbis have been rock solid since I got them 5 years ago, probably overkill for the size of place I'm living in now.

## Services

I self-host a lot of stuff, some of it used daily and some used never. Below are the services I use in order of their importance and usefulness.

### [Vaultwarden](https://github.com/dani-garcia/vaultwarden)

This is my primary password manager and I use it every day. Incredibly easy to get started and very reliable. The vault is backed up to Eriador and offsite storage nightly, with an additional encrypted plain text export manually taken whenever I think about it stored in a different offsite account.

### [Pi-hole](https://github.com/pi-hole/pi-hole)

While I don't access the Pi-hole interface very often, it is the whole network's default DNS server and therefore is used constantly every day. I run this on an LXC container, not a Raspberry Pi.

### [Ampache](https://github.com/ampache/ampache/)

Ampache is a music streaming service I use daily with various Subsonic clients on my laptops and phone. It serves as the single source of truth for my music library.

### [Tiny Tiny RSS](https://tt-rss.org/)

This is a new addition. I've been a happy Feedly customer for years but their new features are exclusively targeted at premium users, and they've started injecting ads into my feed, so tt-rss is replacing Feedly. I have a POC of this up and love it so far, just waiting till after the move to make it official since there will be significant downtime between places. 

### [Nextcloud](https://nextcloud.com/)

Nextcloud is my replacement for Dropbox, I use it primarily for documents I've scanned and need access to quickly from many devices. This is currently very under-utilized and I have a long backlog of things I want to move to Nextcloud, primarily for safety. Nextcloud is backed up nightly to Eriador and offsite storage.

### [Chevereto](https://chevereto.com/)

Photo storage solution, never used. I want to move to something more flexible after the move, haven't decided what yet. All photos from my devices are automatically backed up to Eriador nightly, and in theory should feed the photo storage service directly. Chevereto does not support that use case.

### [Grafana](https://grafana.com/)

I have a few basic dashboards built in Grafana that read from InfluxDB, which collects stats from various servers and containers. Rarely used.

### [Redash](https://redash.io/)

Dashboarding software I use with internal and external databases for dashboards. Haven't touched this in 2 years. 

## Planned projects

I have a few homelab projects planned for this year:

### Self-hosted Youtube

In constant pursuit of my goal of avoiding "The Algorithm," I plan on mirroring Youtube channels I watch to a [Peertube](https://joinpeertube.org/) instance and just use that to watch content chronologically. I'm very perceptible to Youtube rabbit holes, so if I can blackhole `youtube.com` in Pi-hole I'll probably be 30% more productive every day. This project will require a lot of disk space, so expanding my NAS is a prerequisite for this.

### Self-hosted video surveillance

I have used Wyze cameras in my home for years with no complaints, however their security record is [a little lacking lately](https://www.bitdefender.com/blog/hotforsecurity/wyze-cam-vulnerabilities-could-let-attackers-access-the-live-feed-research-finds/) and pushing video from inside my home to the cloud is a privacy nightmare. I plan on setting up [Zoneminder](https://zoneminder.com/) on a new server and installing a few wifi cameras to a dedicated surveillance VLAN with no Internet access.

