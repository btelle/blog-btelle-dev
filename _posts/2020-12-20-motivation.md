---
layout: post
title: Motivation
category: rant
tags: personal motivation goals
---

I've been having a problem with motivation for a while now. I used to be the kind of person who always had at least 2 side projects going at any time and would spend my evenings and weekends working on them whenever I could. I built websites, data pipelines, infrastructure in my homelab, papercraft, electronics.

This year, though, I haven't had that drive. The last thing I worked on with gusto was [Coronavirus Baseball](https://coronavirus-baseball.com), and that was built in a weekend at the very start of the COVID-19 pandemic. Since then, I haven't been reading as much. My commits in Gitlab, where my personal projects live, have been few and far-between. I've been having _ideas_ for projects at about the same rate, I just haven had the drive to get them off the ground.

What could be the reason for this? The elephant in the room, of course, is COVID-19. The pandemic turned this year on its head and disrupted the way we lead our lives. I went from working in an office and having very strict work/life boundaries, to working remotely and having work days blend in to nights blend in to weekends. For some reason, working on a side project seemed to lose its novelty when I was also doing my day job from home.

I've also had an unusually busy year away from the keyboard: I was promoted at work, spent months fixing up and selling our first house, moved from Los Angeles to Nashville with my wife and two dogs, completely furnished and moved in to a new apartment. Even after all of those disruptions settled down in the Fall, my motivation did not return. 

It's not all bad, though. I've worked on [Advent of Code](https://adventofcode.com/) almost every day so far this December. I have a few things in the pipeline for the homelab in the next few months. I'm also starting this blog.

I have another blog, one I haven't updated in over two years. When I started that blog, I had a very specific premise in mind and only wrote posts that fell in line with that premise. I've often wanted to write something to publish that didn't match those criteria, but had no where to publish it so I just didn't write it. That's what this blog is for. 

I'm hoping even small accomplishments like posting a blog here will provide me with motivation to keep going and accomplish more in 2021. Will it work? Maybe, maybe not. Either way, it's worth a try. 
