---
layout: post
title: Modern ETL and Data Warehouse Stack
category: thoughts
tags: data warehouse, etl, scalability
---

Just some thoughts on a modern ETL and data warhouse stack that is:

1. **Managed**. Low overhead, serverless when possible.
2. **Infinitely scalable**. Easily adding processing resources and storage capacity.
3. **Modular**. Easy to add new sources, fault tolerant across pipelines.
4. **Batch**. Batch latency should be scalable, with the ability to shorten batch length to provide sub-hour updates.
5. **Modeled**. The final product should not be soupy lake of data files we need to pre-process before using. 

## Extract

To go fully managed, a service like [Stitch](https://www.stitchdata.com/) provides the easiest solution for extracting data from source systems. I'm personally not a huge fan of a fully-SaaS extract layer since there will always be *something* that isn't supported by the service, so now you're paying the premium for managed and you need to run your own custom extracts as well.

I'd prefer to implement a modular extract using [Singer](https://www.singer.io/), the open source framework Stitch is built on. This would provide most of the benefits of using Stitch (they don't open source some of their more profitable integrations) while also allowing for flexibility for custom integrations using the same tap and target framework.

To make a custom Singer-based extract layer more managed, it could be deployed as container flows to a managed container service like [AWS ECS](https://aws.amazon.com/ecs/) or [GKE](https://cloud.google.com/kubernetes-engine).

## Transform and Load

The most scalable transformation solution is no transform. Storing data in raw extract format (which, if using Singer, should be consistent in terms of file type and structure) and building a data lake on top of it takes no processing. While there are advantages to this solution, I don't think it fits our criteria:

1. Traditional partitioning structures prohibit scaling batch latency. Once you start to partition your data into dates, changing to hourly batches is going to cause problems.
2. Data lakes can be modeled, but relational databases *must* be. Using a warehouse will enforce our final requirement by default.

If data lakes are out, we have two remaining options: transform and load, or load and transform.

### Transform and Load

Using an intermediary processing layer, data is first transformed to the final modeled state, then loaded into the warehouse. The practicality of this approach depends on the desired state of the data. If the model is a traditional star schema with fact and dimension tables, the intermediary step could get very complex.

The managed options for the intermediary processing layer are plentiful. Amazon offers [EMR](https://aws.amazon.com/emr/) for Hadoop and Spark jobs, and [Glue](https://aws.amazon.com/glue/) for a more opinionated approach to data transformation. Google has [Dataproc](https://cloud.google.com/dataproc) for Hadoop and Spark jobs. There are also dedicated Spark-as-a-Service options like [Qubole](https://www.qubole.com/) and [Databricks](https://databricks.com/).

### Load and Transform

On the other hand, raw data can be loaded directly into the warehouse and then transformed into the model. This approach makes creating and updating dimensions and de-duplicating fact tables easier. However, it also requires a lot of processing power on the warehouse side. Luckily there are several warehouses that meet that requirement and more on the market. 

1. **Snowflake**: The hot new thing in data warehouses, Snowflake offers a product that is infinitely scalable in every sense of the term. Data is stored in S3 or GCS, so there is effectively no limit on the amount of data you can store. Processing is done by disparate warehouses that can be sized according to their task: exploratory analysis can be done on an X-Small instance while a full table refresh ETL process uses a Medium instance and a Data Science model runs on a 4X-Large instance. 
2. **BigQuery**: BigQuery is Google's managed data warehouse, and offers all of the features Snowflake has without thinking too hard about them. Storage is unlimited and processing power is effectively unlimited (although there are some limits that become abundantly obvious with large enough jobs.) BigQuery is more opinionated with their SQL dialect and discourage INSERT statements in favor of their API-driven load statements, but the result is the same.
3. **Redshift**: Old reliable, Redshift has been available since 2013. Its age has pros and cons: it is very mature and ubiquitous, if you have a problem with Redshift there are probably 20 Stack Overflow questions with solutions for it. On the other hand, new features are very slow to be released because it has such high expectations for stability. Redshift recently added [RA3](https://aws.amazon.com/about-aws/whats-new/2019/12/amazon-redshift-announces-ra3-nodes-managed-storage/) nodes which allow storage and compute to be decoupled, so clusters no longer need to have nodes added just because storage space is running out. Between RA3 and [Spectrum](https://docs.amazonaws.cn/en_us/redshift/latest/dg/c-using-spectrum.html#c-spectrum-overview), Redshift is keeping up with the competition and is still a viable product for infinite scalability if speed can be sacrificed for stability.

No matter the warehouse, [dbt](https://www.getdbt.com/about/) is making great strides in making ELT cross-platform and pragmatic. dbt would be my tool of choice for transformation logic.

### Why Not Both?

It is rare that any large-scale data platform is going to have every data source fit neatly into either of the two boxes detailed above. The ideal solution would be to support both:

* A Spark- or Hadoop-based pre-processing tool to handle batches of large stand-alone data sets that don't need a lot of modeling. For example, web server logs. The final destination for this raw data need not be the warehouse, some summarizing could be preformed to save space and encourage users to not query the raw event logs using a warehouse that isn't designed for it.
* A dbt-based ELT process to handle batches of relational data that fit into a traditional star schema. For example, orders and transactions. The final destination for this transformed data is the warehouse.

## Orchestration

There are a ton of exciting orchestration tools being built right now because, let's face it, the last generation of scheduling and orchestration tools were not fantastic. Below are the options I am aware of and excited about.

* [Airflow](https://airflow.apache.org/): Airflow rose like a rocket from an open source side-project at AirBnB to a top-level Apache project in a matter of a few years. It is currently the go-to standard for ETL orchestration and has a lot of good things going for it -- a huge community, fantastic motivated maintainers, and a rich set of python tools. Airflow can also be deployed as a managed service using [GCE Cloud Composer](https://cloud.google.com/composer), [Amazon MWAA](https://aws.amazon.com/managed-workflows-for-apache-airflow/), or [Astronomer](https://www.astronomer.io/).
* [Dagster](https://dagster.io/): Dagster is another Python-based orchestration tool that implements some opinionated paradigms about how pipelines should be built. Like Airflow, pipelines are defined in Python rather than using configuration files. Dagster is designed to run the ETL code itself, as opposed to Airflow where the tide is shifting to using container-based operations. Dagster does not have a managed offering that I am aware of, so [deployment would need to be self-managed.](https://docs.dagster.io/deploying/aws) One major pro of Dagster is it is not a scheduling engine, which would lend itself nicely to an event-driven platform in which batches are scheduled at any interval, rather than on a fixed schedule like the rest of these tools.
* [Prefect](https://www.prefect.io/): Prefect is an orchestration tool designed to be run as a managed service. While they do have an open source version, to unlock the potential of the platform the managed version is a must. Like Airflow and Dagster, pipelines are defined in python and pipelines are designed to be coupled with ETL code.
* [Argo](https://argoproj.github.io/projects/argo): Argo is a Kubernetes-native orchestrator and scheduler. Argo uses YAML files for pipeline declaration, which means you lose some of the dynamic generation capabilites of code-based pipelines, but it also means your orchestration is fully decoupled from your ETL code.

Since the orchestration tool ecosystem is being distrupted so rapidly right now, at this moment I would prefer to choose a system where strong decoupling is possible. If your orchestration tool is just being used to schedule tasks in a container environment, it is very easy to move to the next great tool to come along with very low switching cost. 

## Alerting and Monitoring

Even resilient and well-architected pipelines break sometimes. Alerting and monitoring tools are critical to catching problems early and fixing them quickly. 

### Alerting

There are a ton of great alerting options, even some built in to the orchestration tools, which let you flexibly and intelligently send alerts to the team when things are going wrong.

* [Pager Duty](https://www.pagerduty.com/) is a SaaS product that sends notifications to on-call engineers.
* [Amazon SNS](https://aws.amazon.com/sns/) is an API provided by Amazon that sends notifications through email or SMS.
* [Datadog Monitors](https://docs.datadoghq.com/monitors/) is a service provided Datadog that sends alerts when metrics are in dangerous zones. This is a nice complimnetary service to their excellent monitoring offering.

### Monitoring

I'll separate monitoring into two types: monitoring infrastructure, and monitoring quality. 

For monitoring infrastructure, the cloud providers have built-ins which work ([Cloudwatch](https://aws.amazon.com/cloudwatch/), [Cloud Monitoring](https://cloud.google.com/monitoring)), but the player to beat in the space is [Datadog](https://www.datadoghq.com/). In addition to industry-leading monitoring and dashboarding, they offer alerting and log storage and tracing and even monitor [serverless functions](https://docs.datadoghq.com/serverless/).

For monitoring data quality, I like [Sisense](https://www.sisense.com/) to be able to quickly spin up dashboards using pure SQL. [Redash](https://redash.io/) is a self-hosted and open-source option that provides the same functionality, but Sisense is managed and polished.

## Putting it all together

[![Modern ETL and Data Warehouse Diagram](/public/images/modern-etl-warehouse/diagram.png)](/public/images/modern-etl-warehouse/Modern-ETL-Warehouse.pdf)

This diagram illustrates an implementation of a modern ETL platform using AWS services. 

The solution described here meets our criteria:

1. **Managed**. No servers need to be spun up and maintained, everything is serverless, managed, or SaaS.
2. **Infinitely scalable**. Aggressively buying in to Kubernetes and auto-scaling easily lets capacity increase on the extract side, EMR and Snowflake are inherently scalable and even offer auto-scaling options for a no-thought solution.
3. **Modular**. The design of the extract process allows extracts to operate independently. Our two-legged transform allows data to be handled the smartest way for each individual data source.
4. **Batch**. Batch size can be adjusted at the scheduler level and the rest of the pipeline will scale accordingly.
5. **Modeled**. Snowflake tables can be modeled to match the source system or designed in a traditional star or snowflake schema.