---
layout: page
title: About
---

I'm Brandon. I'm a software engineer and a frequent hobbyist. I work on big data platforms for a living. In my free time, I maintain and expand my homelab, build data applications, and read.

This blog is a place for me to write. Things here may not be as polished or focused as what I post on my data-focused blog, [Everyday Data](https://everydaydata.co).